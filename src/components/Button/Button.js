import React from 'react'
import PropTypes from 'prop-types'

import './Button.css'

const Button = props => {
  const { children, onClick, icon } = props

  const iconElement = (
    <i className={`Button__icon ion-ios-${icon}`}></i>
  )

  return (
    <button
      className='Button'
      onClick={onClick}
    >
      {icon && iconElement}
      {children}
    </button>
  )
}

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  icon: PropTypes.string,
}

export default Button
