import React from 'react'
import renderer from 'react-test-renderer'

import Button from './index'

test('Component renders as expected', () => {
  const component = renderer.create(<Button onClick={() => {}}>Ok</Button>)
  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})
