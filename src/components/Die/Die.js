import React from 'react'
import PropTypes from 'prop-types'

import './Die.css'
import dieSprite from './images/die-sprite.png'

class Die extends React.PureComponent {
  state = {
    value: 5,
  }

  _randomInt = (min = 1, max = this.props.sides) => {
    const int = Math.floor(Math.random() * (max - min + 1)) + min
    return int
  }

  _roll = () => {
    if (!this.state.rolling) {
      this.setState({
        rolling: true
      }, () => {
        const int = this._randomInt()
        // Debounce rolling the same number.
        const value = int === this.state.value ? this._randomInt() : int
        this.setState({ value })

        window.setTimeout(() => {
          this.props.onRoll(value)
          this.setState({ rolling: false })
        }, 500)
      })
    }
  }

  render() {
    const { sides, size } = this.props
    const { value } = this.state

    const style = {
      backgroundImage: `url(${dieSprite})`,
      backgroundPositionX: `-${(value - 1) * size}rem`,
      backgroundSize: `${size * sides}rem ${size}rem`,
      height: `${size}rem`,
      width: `${size}rem`,
    }

    return (
      <button
        className='Die'
        disabled={this.state.rolling}
        onClick={this._roll}
        style={style}
        title='Click to roll'
      />
    )
  }
}

Die.propTypes = {
  onRoll: PropTypes.func.isRequired,
  sides: PropTypes.number,
  size: PropTypes.number,
}

Die.defaultProps = {
  sides: 6,
  size: 4,
}

export default Die
