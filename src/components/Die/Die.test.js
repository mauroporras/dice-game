import React from 'react'
import renderer from 'react-test-renderer'

import Die from './index'

test('Component renders as expected', () => {
  const component = renderer.create(<Die onRoll={() => {}} />)
  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})
