import React from 'react'
import PropTypes from 'prop-types'

import './PlayerScores.css'

const PlayerScores = props => {
  const { active, color, globalScore, name, roundScore } = props.player

  const defaultClassName = `PlayerScores PlayerScores--${color}`
  const className = (
    active ? `${defaultClassName} PlayerScores--active` : defaultClassName
  )

  return(
    <div className={className}>
      <h1 className='PlayerScores__name'>{name}</h1>
      <h2 className='PlayerScores__global-score' title='Global score'>
        {globalScore}
      </h2>
      <div className='PlayerScores__round-score-wrapper'>
        <h3 className='PlayerScores__round-score-title'>round score</h3>
        <h3 className='PlayerScores__round-score'>{roundScore}</h3>
      </div>
    </div>
  )
}

PlayerScores.propTypes = {
  player: PropTypes.object.isRequired,
}

export default PlayerScores
