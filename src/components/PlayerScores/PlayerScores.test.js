import React from 'react'
import renderer from 'react-test-renderer'

import PlayerScores from './index'

test('Component renders as expected', () => {
  const component = renderer.create(<PlayerScores player={{}} />)
  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})
