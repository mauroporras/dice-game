import React from 'react'

import Match from '../Match/'

import './App.css'

class App extends React.Component {
  state = {
    globalScoreToWin: 100,
    players: [
      {
        active: true,
        color: 'primary',
        id: 1,
        name: 'player 1',
      },
      {
        color: 'secondary',
        id: 2,
        name: 'player 2',
      },
    ],
  }

  render() {
    const { globalScoreToWin, players } = this.state
    return (
      <div className="App">
        <Match globalScoreToWin={globalScoreToWin} players={players} />
      </div>
    )
  }
}

export default App
