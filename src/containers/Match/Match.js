import React from 'react'
import PropTypes from 'prop-types'
import R from 'ramda'

import Button from '../../components/Button/'
import Die from '../../components/Die/'
import PlayerScores from '../../components/PlayerScores/'

import './Match.css'

class Match extends React.Component {
  state = {
    showInstructions: true,
  }

  _switchPlayers = players => [
    {
      ...players[1],
      active: true,
    },
    {
      ...players[0],
      active: false,
    },
  ]

  _addToScore = (players, field, value) => {
    const activePlayer = players[0]
    const fullField = field + 'Score'
    return [
      {
        ...activePlayer,
        [fullField]: activePlayer[fullField] + value,
      },
      {
        ...players[1],
      },
    ]
  }

  _resetScores = (players, ...scoresTypes) => {
    const newScores = R.reduce(
      (a, v) => ({ ...a, [v + 'Score']: 0 }),
      {},
      scoresTypes
    )
    return R.map(e => ({ ...e, ...newScores }), players)
  }

  _newGame = () => {
    this.setState((__, props) => ({
      players: this._resetScores(R.clone(props.players), 'round', 'global'),
      winner: null,
    }))
  }

  _dieRolled = value => {
    this.setState(prevState => {
      const { players } = prevState
      const newPlayers =
        value === 1
          ? this._switchPlayers(this._resetScores(players), 'round')
          : this._addToScore(players, 'round', value)
      return { players: newPlayers }
    })
  }

  _findWinner = (players, globalScoreToWin) =>
    R.find(e => e.globalScore >= globalScoreToWin, players)

  _hold = () => {
    this.setState((prevState, props) => {
      const { players } = prevState
      const newPlayers = this._resetScores(
        this._addToScore(players, 'global', players[0].roundScore),
        'round'
      )

      const winner = this._findWinner(newPlayers, props.globalScoreToWin)

      return {
        players: winner ? newPlayers : this._switchPlayers(newPlayers),
        winner,
      }
    })
  }

  _hideInstructions = () => {
    this.setState({ showInstructions: false })
  }

  componentWillMount() {
    this._newGame()
  }

  render() {
    const { players, showInstructions, winner } = this.state
    const { globalScoreToWin } = this.props

    const sortedPlayers = R.sort((a, b) => a.id - b.id, players)
    const hasWinner = Boolean(winner)

    return (
      <div>
        {showInstructions &&
          <div className="Match__instructions">
            The first player to reach {globalScoreToWin} global score, wins.
            <Button icon="close-outline" onClick={this._hideInstructions} />
          </div>}

        <div className="Match" role="main">
          <div className="Match__left-side">
            <PlayerScores player={sortedPlayers[0]} />
          </div>
          <div className="Match__right-side">
            <PlayerScores player={sortedPlayers[1]} />
          </div>
          <div className="Match__dashboard" role="navigation">
            <Button icon="plus-outline" onClick={this._newGame}>
              new game
            </Button>
            {hasWinner &&
              <strong className="Match__winner-notice">
                {winner.name} wins!
              </strong>}
            {hasWinner || <Die onRoll={this._dieRolled} size={7} />}
            {hasWinner ||
              <Button icon="download-outline" onClick={this._hold}>
                hold
              </Button>}
          </div>
        </div>
      </div>
    )
  }
}

Match.propTypes = {
  globalScoreToWin: PropTypes.number.isRequired,
  players: PropTypes.array.isRequired,
}

export default Match
