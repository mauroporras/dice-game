import React from 'react'
import renderer from 'react-test-renderer'

import Match from './index'

describe('Match', () => {
  it('renders as expected', () => {
    const component = renderer.create(
      <Match globalScoreToWin={80} players={[{}, {}]} />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('starts showing instructions', () => {
    const match = new Match()
    expect(match.state.showInstructions).toBeTruthy()
  })

  it('switches players', () => {
    const match = new Match()

    const player1 = { name: 'foo' }
    const player2 = { name: 'bar' }
    const playersBefore = [player1, player2]
    const playersAfter = [
      { ...player2, active: true },
      { ...player1, active: false },
    ]

    expect(match._switchPlayers(playersBefore)).toEqual(playersAfter)
  })

  it('resets scores', () => {
    const match = new Match()
    const players = [
      { roundScore: 1, globalScore: 2 },
      { roundScore: 3, globalScore: 4 },
    ]

    expect(match._resetScores(players, 'round')).toEqual([
      { roundScore: 0, globalScore: 2 },
      { roundScore: 0, globalScore: 4 },
    ])

    expect(match._resetScores(players, 'global')).toEqual([
      { roundScore: 1, globalScore: 0 },
      { roundScore: 3, globalScore: 0 },
    ])

    expect(match._resetScores(players, 'round', 'global')).toEqual([
      { roundScore: 0, globalScore: 0 },
      { roundScore: 0, globalScore: 0 },
    ])
  })
})
